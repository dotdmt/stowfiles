KOPTS = { noremap = true, }
SKOPTS = { noremap = true, silent = true }

-- local map = vim.api.nvim_set_keymap

vim.g.mapleader = " "
vim.g.maplocalleader = " "
vim.keymap.set("", "<Space><Space>", "<Nop>", KOPTS)
vim.keymap.set("", "<Space><C-c>", "<Nop>", KOPTS)

vim.keymap.set({ "n", "i", }, "<F1>", "<Nop>", KOPTS)

vim.keymap.set("n", "Q", "<Nop>", KOPTS)
vim.keymap.set({ "n", "i" }, "<C-c>", "<Esc><Esc>", KOPTS)
vim.keymap.set({ "n", "i" }, "<C-s>", "<Esc><Cmd>w<CR><Esc>", KOPTS)
vim.keymap.set("i", "<C-a>", "<Esc><S-a>", KOPTS)
vim.keymap.set("n", "<C-q>", "<Cmd>quit<CR>", SKOPTS)

-- vim.keymap.set("t", "<C-n>", "<C-\\><C-n>", KOPTS)

-- vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz", KOPTS)
-- vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz", KOPTS)

vim.keymap.set("n", "<C-h>", "<C-w>h", KOPTS)
vim.keymap.set("n", "<C-k>", "<C-w>k", KOPTS)
vim.keymap.set("n", "<C-l>", "<C-w>l", KOPTS)
vim.keymap.set("n", "<C-j>", "<C-w>j", KOPTS)

-- move text up and down NOTE: DO NOT REPLACE ":m" WITH "<CMD>m"
vim.keymap.set("v", "<C-j>", ":m '>+1<CR>gv=gv", KOPTS)
vim.keymap.set("v", "<C-k>", ":m '<-2<CR>gv=gv", KOPTS)

vim.keymap.set("n", "<leader>lz", "<Cmd>Lazy<CR>", KOPTS)
-- vim.keymap.set("n", "<leader>li", "<Cmd>LspInfo<CR>", KOPTS)

-- Navigate buffers
vim.keymap.set("n", "<S-l>", "<Cmd>bnext<CR>", SKOPTS)
vim.keymap.set("n", "<S-h>", "<Cmd>bprevious<CR>", SKOPTS)
vim.keymap.set("n", "<S-w>", "<Cmd>bdelete<CR>", SKOPTS)

-- Stay in indent mode
vim.keymap.set("v", ">", ">gv^", KOPTS)
vim.keymap.set("v", "<", "<gv^", KOPTS)

-- PRIME
vim.keymap.set("n", "J", "mzJ`z", KOPTS)
vim.keymap.set("x", "p", [["_dP"]], KOPTS)

vim.keymap.set("n", "n", "nzzzv", KOPTS)
vim.keymap.set("n", "N", "Nzzzv", KOPTS)

vim.keymap.set("n", "<M-k>", "<C-u>", SKOPTS)
vim.keymap.set("n", "<M-j>", "<C-d>", SKOPTS)
