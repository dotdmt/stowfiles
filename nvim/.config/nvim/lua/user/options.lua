local vo = vim.opt
-- help vim.opt

-- vo.showtabline = 2
vo.number = true
vo.relativenumber = true
vo.termguicolors = true
-- vo.cmdheight = 1
vo.completeopt = { "menuone", "noselect" }
vo.hlsearch = false
vo.incsearch = true
vo.ignorecase = true
vo.mouse = "a"
vo.pumheight = 10
vo.showmode = false
vo.smartcase = true
vo.smartindent = true
vo.expandtab = true
vo.shiftwidth = 4
vo.tabstop = 4
vo.numberwidth = 4
vo.cursorline = true
vo.signcolumn = "yes"
vo.wrap = false
vo.linebreak = true
vo.laststatus = 3

vo.scrolloff = 30

vo.backup = false
vo.undodir = os.getenv("HOME") .. "/.cache/nvim/undodir"
vo.undofile = true

vim.g.netrw_banner = 0
vim.g.netrw_liststyle = 3

-- https://github.com/ThePrimeagen/init.lua/raw/master/lua/theprimeagen/init.lua
-- highlight yanked text
local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd
local yank_group = augroup("HighlightYank", {})

autocmd("TextYankPost", {
    group = yank_group,
    pattern = "*",
    callback = function()
        vim.highlight.on_yank({
            higroup = "IncSearch",
            timeout = 125,
        })
    end,
})
