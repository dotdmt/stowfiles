#  _________  _   _ ____   ____
# |__  / ___|| | | |  _ \ / ___|
#   / /\___ \| |_| | |_) | |
#  / /_ ___) |  _  |  _ <| |___
# /____|____/|_| |_|_| \_\\____|
#

# Basics
unsetopt beep
setopt autocd
bindkey -v

## Enable tab complete menu
autoload -U compinit && compinit -u
zstyle ':completion:*' menu select

## Case insensitive
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files

# IMPORTANT DEFAULTS
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'

# Refresh config
alias zshrc='. ~/.zshrc'

# EXPORTS
export MANPAGER='nvim -M +Man!'
export EDITOR='nvim'

# Aliases
if [ -f ~/.zsh_aliases ]; then
    . $HOME/.zsh_aliases
fi

if [ -f ~/.local/bin/starship ]; then
    eval "$(bwrap --unshare-all --new-session --ro-bind / / --ro-bind $HOME/.config/starship.toml $HOME/.config/starship.toml starship init zsh)"
fi

if [ -x ~/.zsh/ ]; then
    . ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
    . ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

if [[ "$(tty)" = "not a tty" ]]; then
    export TERM=xterm-256color
fi

# bash syntax highlighting is better
# vim: ft=bash
